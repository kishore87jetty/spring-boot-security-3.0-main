package com.java.controller;

import com.java.dto.AuthRequest;
import com.java.dto.JwtResponse;
import com.java.dto.Product;
//import com.javatechie.entity.UserInfo;
import com.java.dto.RefreshTokenRequest;
import com.java.entity.RefreshToken;
import com.java.entity.UserInfo;
import com.java.service.JwtService;
import com.java.service.ProductService;
import com.java.service.RefreshTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductController {

    @Autowired
    private ProductService service;

    @Autowired
    private JwtService jwtService;

    @Autowired
    private AuthenticationManager authenticationManager;


    @Autowired
    private RefreshTokenService refreshTokenService;

    @GetMapping("/welcome")
    public String welcome() {
        return "Welcome this endpoint is not secure";
    }

    @PostMapping("/new")
    public String addNewUser(@RequestBody UserInfo userInfo){
        return service.addUser(userInfo);
    }

    @GetMapping("/all")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public List<Product> getAllTheProducts() {
        return service.getProducts();
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public Product getProductById(@PathVariable int id) {
        return service.getProduct(id);
    }


    @PostMapping("/authenticate")
    public ResponseEntity<JwtResponse> AuthenticateAndGetToken(@RequestBody AuthRequest authRequest){

        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authRequest.getUserName(), authRequest.getPassword()));
        if(authentication.isAuthenticated()) {
            RefreshToken refreshToken = refreshTokenService.createRefreshToken(authRequest.getUserName());
            JwtResponse jwtResponse = JwtResponse.builder()
                    .accessToken(jwtService.gererateToken(authRequest.getUserName()))
                    .token(refreshToken.getToken())
                    .build();

            return new ResponseEntity<>(jwtResponse,new HttpHeaders(), HttpStatus.OK);

        }else {
            throw  new UsernameNotFoundException("invalid User request !") ;
        }
    }

    @PostMapping("/refreshToken")
    public JwtResponse refreshToken(@RequestBody RefreshTokenRequest tokenRequest){

        return  refreshTokenService.findByToken(tokenRequest.getToken())
                .map(refreshTokenService::vertifyExpiration)
                .map(RefreshToken::getUserInfo)
                .map(userInfo -> {

                    String accessToken = jwtService.gererateToken(userInfo.getName());
                    return JwtResponse.builder().accessToken(accessToken)
                            .token(tokenRequest.getToken())
                            .build();
                }).orElseThrow(()-> new RuntimeException("Refresh token is not in database"));

    }

}
