package com.java.service;

import com.java.entity.RefreshToken;
import com.java.repository.RefreshTokenRepository;
import com.java.repository.UserInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

@Service
public class RefreshTokenService {

    @Autowired
    private RefreshTokenRepository refreshTokenRepository;

    @Autowired
    private UserInfoRepository userInfoRepository;

    @Value("${jwt.token.refresh.expiry}")
    private String refreshTokenExpiry;

    public RefreshToken createRefreshToken(String userName){

        RefreshToken refresh = RefreshToken.builder()
                .userInfo(userInfoRepository.findByName(userName).get())
                .token(UUID.randomUUID().toString())
                .expiryDate(Instant.now().plusMillis(Long.parseLong(refreshTokenExpiry))) //10 mins
                .build();
        return refreshTokenRepository.save(refresh);


    }


    public Optional<RefreshToken> findByToken(String token){

        return refreshTokenRepository.findByToken(token);
    }

    public RefreshToken vertifyExpiration(RefreshToken refreshToken){

        if(refreshToken.getExpiryDate().compareTo(Instant.now())<0){
            refreshTokenRepository.delete(refreshToken);
            throw new RuntimeException(refreshToken.getToken() +
                    " Refresh token was expired. Please make a new sign in request");
        }
        return refreshToken;
    }
}
